# syntax=docker/dockerfile:experimental

ARG STAND
ARG ROLE_ID
ARG SECRET_ID

#------------------------------------------
# test
#------------------------------------------
FROM alpine:3.12.0 AS test

ARG STAND
ARG ROLE_ID
ARG SECRET_ID

RUN apk add \
    nodejs \
    npm \
    openssh \
    build-base \
    python2 \
    pkgconfig \
    git

# Setup ssh for getting dependencies from git.epam.com
RUN mkdir -p -m 0600 ~/.ssh && ssh-keyscan git.epam.com >> ~/.ssh/known_hosts

WORKDIR /usr/recording-preview
COPY ./ ./

RUN npm set unsafe-perm true
RUN --mount=type=ssh,id=default npm i && \
                                npm run lint
CMD ["/bin/sh", "-c", "npm run test; \
      if [ \"$?\" != \"0\" ]; then \
        cp -v junit.xml coverage-out/; \
        cp -r coverage/* coverage-out/ ;\
        exit 1; \
      else \
        cp -v junit.xml coverage-out/; \
        cp -r coverage/* coverage-out/ ; \
      fi"]

###########################################
# builder
###########################################
FROM alpine:3.12.0 AS builder

ARG STAND
ARG ROLE_ID
ARG SECRET_ID

RUN apk add \
    nodejs \
    npm \
    openssh \
    build-base \
    python2 \
    pkgconfig \
    git

# Setup ssh for getting dependencies from git.epam.com
RUN mkdir -p -m 0600 ~/.ssh && ssh-keyscan git.epam.com >> ~/.ssh/known_hosts

WORKDIR /usr/recording-preview
COPY ./ ./

RUN npm set unsafe-perm true
RUN --mount=type=ssh,id=default npm i && npm run build
RUN --mount=type=ssh,id=default npx 'git+ssh://git@git.epam.com:epm-s2m/env-config.git' -- -module recording-preview -stand ${STAND} -path ./config/default.json -role_id ${ROLE_ID} -secret_id ${SECRET_ID}


###########################################
# recording-preview
###########################################
FROM alpine:3.12.0

RUN apk add \
    nodejs \
    npm

# Install forever
RUN npm i -g forever

WORKDIR /usr/set2meet
# Copy Config and recording-preview
COPY --from=builder /usr/recording-preview/config ./config/
COPY --from=builder /usr/recording-preview ./recording-preview

ENTRYPOINT ["forever", "recording-preview/dist/index.js"]
