export enum LogModule {
  Index = 'Index',
  Participant = 'Participant',
  Realm = 'Realm',
  Creation = 'Creation',
  Removal = 'Removal',
  Download = 'Download',
  User = 'User',
  StopRecording = 'StopRecording',
  InitQueue = 'InitQueue',
  Report = 'Report',
}
