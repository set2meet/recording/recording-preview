export enum LogErrorCode {
  E_INDEX = 'E_INDEX',
  E_REALM = 'E_REALM',
  E_INIT_QUEUE = 'E_INIT_QUEUE',
}
