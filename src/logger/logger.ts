import { ServerLogger, S2MServices } from '@s2m/logger';
import { LogErrorCode } from './LogErrorCode';
import { LogModule } from './LogModule';

const appLogger: ServerLogger<LogModule, LogErrorCode> = new ServerLogger(S2MServices.RecordingProcessing);

const indexLogger = appLogger.createBoundChild(LogModule.Index);
const participantLogger = appLogger.createBoundChild(LogModule.Participant);
const realmLogger = appLogger.createBoundChild(LogModule.Realm);
const creationLogger = appLogger.createBoundChild(LogModule.Creation);
const removalLogger = appLogger.createBoundChild(LogModule.Removal);
const downloadlLogger = appLogger.createBoundChild(LogModule.Download);
const userLogger = appLogger.createBoundChild(LogModule.User);
const stopLogger = appLogger.createBoundChild(LogModule.StopRecording);
const initLogger = appLogger.createBoundChild(LogModule.InitQueue);
const reportLogger = appLogger.createBoundChild(LogModule.Report);

export default {
  index: indexLogger,
  participant: participantLogger,
  realm: realmLogger,
  creation: creationLogger,
  removal: removalLogger,
  download: downloadlLogger,
  user: userLogger,
  stop: stopLogger,
  init: initLogger,
  report: reportLogger,
};
