import { IRecordingDb } from '@s2m/recording-db';
import { mock } from 'jest-mock-extended';

export default mock<IRecordingDb>();
