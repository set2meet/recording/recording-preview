import { Next } from 'koa';
import api from '@s2m/resource-management';
import { constants } from 'http2';
import { TAppContext } from '../types';

const checkUserPermissions = async (ctx: TAppContext, next: Next): Promise<void> => {
  const token = ctx.state.token;
  const realm = ctx.state.realm;
  const recordId = ctx.params.recordId;
  const hasPermissions = await api(realm).hasDeletePermissions(realm, recordId, token);
  const errorMessage = "You don't have delete permission.";

  ctx.assert(hasPermissions, constants.HTTP_STATUS_FORBIDDEN, errorMessage);

  await next();
};

export default checkUserPermissions;
