import https from 'https';
import http from 'http';
import { PassThrough, Stream } from 'stream';

const getAppropriateHttpClient = (signedUrl: string) => {
  return signedUrl.startsWith('https') ? https : http;
};

const getStreamReaderFromUrl = (signedUrl: string): Stream => {
  const passThrough = new PassThrough();
  const httpClient = getAppropriateHttpClient(signedUrl);

  httpClient.get(signedUrl, (resp) => {
    resp.pipe(passThrough);
  });

  return passThrough;
};

export default getStreamReaderFromUrl;
