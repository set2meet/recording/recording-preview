import getStreamReaderFromUrl from '../getStreamReaderFromUrl';
import http from 'http';
import https from 'https';
import { PassThrough } from 'stream';

/* eslint-disable @typescript-eslint/no-unsafe-return,
@typescript-eslint/no-unsafe-call
*/

jest.mock('http', () => ({ get: jest.fn((_, fn) => fn(new PassThrough())) }));
jest.mock('https', () => ({ get: jest.fn((_, fn) => fn(new PassThrough())) }));

describe('getStreamReaderFromUrl', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  test('https client', () => {
    getStreamReaderFromUrl('https://host/path1');

    expect(https.get).toBeCalled();
  });

  test('http client', () => {
    getStreamReaderFromUrl('http://host/path1');

    expect(http.get).toBeCalled();
  });
});
