import { Context, Next } from 'koa';

const checkHealth = async (ctx: Context, next: Next): Promise<void> => {
  ctx.body = 'ok';

  await next();
};

export default checkHealth;
