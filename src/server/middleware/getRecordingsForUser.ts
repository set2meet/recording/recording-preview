import { Next } from 'koa';
import api, { TResourceDesc, GroupName } from '@s2m/resource-management';
import logger from '../../logger/logger';
import { TRecordItem } from '@s2m/recording-db';
import db from '../db';
import { TAppContext } from '../types';

const userLogger = logger.user;

const isRecordExists = (recordItem: Partial<TRecordItem>): boolean => !!recordItem.createdBy;

const getRecordWithAdditionalInf = async (res: TResourceDesc): Promise<TRecordItem> => {
  const record = await db.getRecord(res.name);
  const isOwner = res.groups.includes(GroupName.owners);
  const isViewer = res.groups.includes(GroupName.viewers);

  record.meta = {
    isOwner,
    isViewer,
  };

  return record;
};

const getRecordingsForUser = async (ctx: TAppContext, next: Next): Promise<void> => {
  const realm = ctx.state.realm;
  const userId = ctx.params.userId;
  const resourceNames = await api(realm).fetchResourceNamesByUserId(realm, userId);
  const recordItems = (await Promise.all(resourceNames.map(getRecordWithAdditionalInf))).filter(isRecordExists);

  userLogger.debug(`user ${userId} resources `, recordItems);

  ctx.body = recordItems;

  await next();
};

export default getRecordingsForUser;
