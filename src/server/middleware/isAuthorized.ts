import api from '@s2m/resource-management';
import { Next } from 'koa';
import { constants } from 'http2';
import { TAppContext } from '../types';

const isAuthorized = async (ctx: TAppContext, next: Next): Promise<void> => {
  const realm = ctx.state.realm;
  const token = ctx.state.token;
  const isValid = await api(realm).validateToken(realm, token);
  const errorMessage = "You aren't authorized.";

  ctx.assert(isValid, constants.HTTP_STATUS_UNAUTHORIZED, errorMessage);

  await next();
};

export default isAuthorized;
