import { Next } from 'koa';
import { getStorage } from '@s2m/cloud';
import { IQuery, TAppContext } from '../types';
import getStreamReaderFromUrl from './utils/getStreamReaderFromUrl';
import mime from 'mime-types';
import logger from '../../logger/logger';

const downloadlLogger = logger.download;

const downloadRecording = async (ctx: TAppContext, next: Next): Promise<void> => {
  const query = ctx.query as IQuery;
  const type = mime.lookup(query.uri) || mime.types.bin;
  const signedUrl: string = await getStorage().signedUrl(query.uri);

  downloadlLogger.info(`mime = ${type}, signed url = ${signedUrl}`);

  ctx.body = getStreamReaderFromUrl(signedUrl);
  ctx.type = type;

  await next();
};

export default downloadRecording;
