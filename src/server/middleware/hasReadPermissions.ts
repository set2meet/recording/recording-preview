import { Next } from 'koa';
import { TAppContext, IQuery } from '../types';
import api from '@s2m/resource-management';
import { constants } from 'http2';

const getRecordIdFromUrl = (uri: string): string =>
  uri
    .split('/')
    .pop()
    .split('.')[0];

const checkUserPermissions = async (ctx: TAppContext, next: Next): Promise<void> => {
  const realm = ctx.state.realm;
  const token = ctx.state.token;
  const query = ctx.query as IQuery;
  const recordId = getRecordIdFromUrl(query.uri);
  const hasPermissions = await api(realm).hasReadPermissions(realm, recordId, token);
  const errorMessage = "You don't have read permission.";

  ctx.assert(hasPermissions, constants.HTTP_STATUS_FORBIDDEN, errorMessage);

  await next();
};

export default checkUserPermissions;
