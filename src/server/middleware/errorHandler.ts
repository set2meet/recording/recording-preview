import { constants } from 'http2';
import { Context, Next } from 'koa';

/* eslint-disable @typescript-eslint/no-unsafe-assignment,
@typescript-eslint/no-unsafe-member-access
*/

const errorHandler = async (ctx: Context, next: Next): Promise<void> => {
  try {
    await next();
  } catch (e) {
    ctx.status = e.status || constants.HTTP_STATUS_INTERNAL_SERVER_ERROR;
    ctx.body = {
      message: e.message,
    };
    ctx.app.emit('error', e, ctx);
  }
};

export default errorHandler;
