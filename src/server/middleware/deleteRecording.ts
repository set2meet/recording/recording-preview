import { Next } from 'koa';
import { getQueue, TaskActionType } from '@s2m/cloud';
import { RecordItemStatus, RecordingMessageType, getRecordingSubPub } from '@s2m/recording-db';
import logger from '../../logger/logger';
import { constants } from 'http2';
import db from '../db';
import getParticipantsByRecordId from '../utils/getParticipantsByRecord';
import { TAppContext } from '../types';
import { IResourceManagementWrapper } from 'env-config';

const removalLogger = logger.removal;

const deleteRecording = (resourceManagementConfig: IResourceManagementWrapper) => async (
  ctx: TAppContext,
  next: Next
): Promise<void> => {
  const recordId = ctx.params.recordId;
  const record = await db.getRecord(recordId);
  const participants = await getParticipantsByRecordId(resourceManagementConfig, record);
  const patch = {
    status: RecordItemStatus.removing,
  };

  await db.updateRecord(record.id, patch);

  removalLogger.info(`Create queue Remove task for ${record.id} ${record.src}`);

  await getQueue().createRecordTask({
    id: record.id,
    src: record.src,
    action: TaskActionType.Remove,
  });

  await getRecordingSubPub().publishToChannel({
    type: RecordingMessageType.RECORDING_UPDATED,
    record: { ...record, ...patch },
    participants,
  });

  ctx.status = constants.HTTP_STATUS_OK;

  await next();
};

export default deleteRecording;
