import downloadRecording from '../downloadRecording';
import { getStorage } from '@s2m/cloud';
import getStreamReaderFromUrl from '../utils/getStreamReaderFromUrl';
import { TAppContext } from '../../types';
import { mock } from 'jest-mock-extended';

jest.mock('../utils/getStreamReaderFromUrl');
jest.mock('@s2m/cloud');

describe('test downloadRecording', () => {
  test('test default mime type', async () => {
    (getStreamReaderFromUrl as jest.Mock).mockReturnValue('fileStream1');
    (getStorage as jest.Mock).mockImplementation(() => ({
      signedUrl: (uri: string): string => `async/${uri}`,
    }));

    const next = jest.fn();
    const ctx = mock<TAppContext>();

    ctx.query = { uri: '/uri' };

    await downloadRecording(ctx, next);

    expect(ctx.body).toBe('fileStream1');
    expect(ctx.type).toBe('application/octet-stream');
  });
});
