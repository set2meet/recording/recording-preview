import errorHandler from '../errorHandler';
import { TAppContext } from '../../types';
import { mockDeep } from 'jest-mock-extended';
import { constants } from 'http2';

jest.mock('../utils/getStreamReaderFromUrl');
jest.mock('@s2m/cloud');

describe('errorHandler', () => {
  test('test', async () => {
    const error = {
      status: constants.HTTP_STATUS_INTERNAL_SERVER_ERROR,
      message: 'error',
    };
    const next = jest.fn().mockRejectedValue(error);
    const ctx = mockDeep<TAppContext>();

    await errorHandler(ctx, next);

    expect(ctx.status).toBe(constants.HTTP_STATUS_INTERNAL_SERVER_ERROR);
    expect(ctx.body).toEqual({
      message: 'error',
    });
    // eslint-disable-next-line @typescript-eslint/unbound-method
    expect(ctx.app.emit).toBeCalledWith('error', error, ctx);
  });
});
