import { Next } from 'koa';
import db from '../db';
import logger from '../../logger/logger';
import { constants } from 'http2';
import api from '@s2m/resource-management';
import { TAppContext } from '../types';

const participantLogger = logger.participant;

const addParticipantToRecording = async (ctx: TAppContext, next: Next): Promise<void> => {
  const recordId = ctx.params.recordId;
  const userId = ctx.params.userId;
  const realm = ctx.state.realm;
  const record = await db.getRecord(recordId);

  participantLogger.info(`add participant ${userId} to record id=${recordId}`);

  await api(realm).assocResourceWithUsers(realm, record.id, {
    viewers: [userId],
  });

  ctx.status = constants.HTTP_STATUS_OK;

  await next();
};

export default addParticipantToRecording;
