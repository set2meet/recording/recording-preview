import { bearerToken, parseToken } from '@s2m/resource-management';
import { Next } from 'koa';
import logger from '../../logger/logger';
import { LogErrorCode } from '../../logger/LogErrorCode';
import { TAppContext } from '../types';
import { constants } from 'http2';
import { IResourceManagementWrapper } from 'env-config';

const realmLogger = logger.realm;

const AUTHORIZATION_HEADER = 'Authorization';

//eslint-disable-next-line max-statements
const appendRealm = (resourceManagementConfig: IResourceManagementWrapper) => async (
  ctx: TAppContext,
  next: Next
): Promise<void> => {
  try {
    const token = bearerToken(ctx.get(AUTHORIZATION_HEADER));
    const parsedToken = parseToken(token);
    const realm = resourceManagementConfig.getRealm(parsedToken.realmName);

    ctx.state.parsedToken = parsedToken;
    ctx.state.realm = realm;
    ctx.state.token = token;

    realmLogger.debug('realm', { realm });
    realmLogger.debug('parsedToken', { parsedToken });
  } catch (e) {
    realmLogger.error(LogErrorCode.E_REALM, 'error');

    ctx.throw(constants.HTTP_STATUS_BAD_REQUEST, 'Incorrect token.');
  }

  await next();
};

export default appendRealm;
