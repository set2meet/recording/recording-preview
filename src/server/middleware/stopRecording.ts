import { Next } from 'koa';
import { RecordItemStatus } from '@s2m/recording-db';
import logger from '../../logger/logger';
import { constants } from 'http2';
import { TAppContext } from '../types';
import { RecordingMessageType, getRecordingSubPub } from '@s2m/recording-db';
import db from '../db';
import getParticipantsByRecordId from '../utils/getParticipantsByRecord';
import { IResourceManagementWrapper } from 'env-config';

const stopLogger = logger.stop;

const stopRecording = (resourceManagementConfig: IResourceManagementWrapper) => async (
  ctx: TAppContext,
  next: Next
): Promise<void> => {
  const recordId = ctx.params.recordId;

  stopLogger.info(`stop record id=${recordId}`);

  await db.updateRecord(recordId, {
    status: RecordItemStatus.processing,
  });

  const record = await db.getRecord(recordId);
  const participants = await getParticipantsByRecordId(resourceManagementConfig, record);

  await getRecordingSubPub().publishToChannel({
    type: RecordingMessageType.RECORDING_UPDATED,
    record,
    participants,
  });

  ctx.status = constants.HTTP_STATUS_OK;

  await next();
};

export default stopRecording;
