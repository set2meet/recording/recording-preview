import { Next } from 'koa';
import { RecordItemStatus } from '@s2m/recording-db';
import db from '../db';
import logger from '../../logger/logger';
import { constants } from 'http2';
import api from '@s2m/resource-management';
import { TAppContext } from '../types';
import dateFormat from 'dateformat';

const creationLogger = logger.creation;

const RECORD_CREATED_AT_FORMAT = 'yyyy-mm-dd HH:MM';

// eslint-disable-next-line max-statements
const createRecording = async (ctx: TAppContext, next: Next): Promise<void> => {
  const realm = ctx.state.realm;
  const parsedToken = ctx.state.parsedToken;
  const userId = parsedToken.payload.sub;
  const startedAt = new Date();
  const recordId = ctx.params.recordId;
  const formattedDate = dateFormat(startedAt, RECORD_CREATED_AT_FORMAT);
  const userName = parsedToken.payload.name;
  const name = `${formattedDate} Recording - ${userName}`;
  const realmName = realm.realmName;
  //eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
  const roomId = ctx.request.body.roomId as string;

  creationLogger.info(`Create new record id=${recordId}, name=${name}`);

  await db.updateRecord(recordId, {
    createdAt: startedAt,
    createdBy: userId,
    roomId,
    realmName,
    status: RecordItemStatus.recording,
    name,
  });

  await api(realm).assocResourceWithUsers(realm, recordId, {
    owners: [userId],
  });

  ctx.status = constants.HTTP_STATUS_OK;

  await next();
};

export default createRecording;
