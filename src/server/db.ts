/* istanbul ignore file */
import { getRecordingDb } from '@s2m/recording-db';

export default getRecordingDb();
