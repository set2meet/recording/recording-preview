/* istanbul ignore file */
import http from 'http';
import https from 'https';
import createApp from './app';
import logger from '../logger/logger';
import { LogErrorCode } from '../logger/LogErrorCode';
import initQueueListener from './tasks/initQueueListener';
import { IRecordingPreviewConfig, readConfig } from 'env-config';

const indexLogger = logger.index;

const PORT = 3000;

const createServer = async () => {
  const config = await readConfig<IRecordingPreviewConfig>();
  const app = createApp(config);

  app.on('error', function(err: Error) {
    indexLogger.error(LogErrorCode.E_INDEX, err.message);
  });
  let recordLifetime = config.recording?.record?.lifetime;

  if (typeof recordLifetime === 'string') recordLifetime = parseInt(recordLifetime, 10);

  const server =
    process.env.NODE_ENV === 'development'
      ? // eslint-disable-next-line @typescript-eslint/no-misused-promises
        https.createServer(config.httpsOptions, app.callback())
      : // eslint-disable-next-line @typescript-eslint/no-misused-promises
        http.createServer(app.callback());

  server.listen(PORT, () => {
    initQueueListener(config, recordLifetime);

    indexLogger.info(`Server started on ${PORT}, env ${process.env.NODE_ENV}`);
  });
};

void createServer();
