/* istanbul ignore file */
import downloadRecordingRoute from './routers/downloadRecordingRoute';
import deleteRecordingRoute from './routers/deleteRecordingRoute';
import recordingIdsByUserRoute from './routers/recordingIdsByUserRoute';
import healthRoute from './routers/healthRoute';
import createRecordingRoute from './routers/createRecordingRoute';
import stopRecordingRoute from './routers/stopRecordingRoute';
import addParticipantToRecordingRoute from './routers/addParticipantToRecordingRoute';
import createApp from './createApp';
import { IRecordingPreviewConfig } from 'env-config';

const registerRoutes = (config: IRecordingPreviewConfig) => {
  return [
    healthRoute,
    downloadRecordingRoute(config.resourceManagement),
    deleteRecordingRoute(config.resourceManagement),
    createRecordingRoute(config.resourceManagement),
    recordingIdsByUserRoute(config.resourceManagement),
    stopRecordingRoute(config.resourceManagement),
    addParticipantToRecordingRoute(config.resourceManagement),
  ];
};

export default createApp(registerRoutes);
