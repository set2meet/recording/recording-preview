import { TTaskBody } from '@s2m/cloud';
import { RecordItemStatus, RecordingMessageType, getRecordingSubPub } from '@s2m/recording-db';
import db from '../db';
import getParticipantsByRecordId from '../utils/getParticipantsByRecord';
import { IResourceManagementWrapper } from 'env-config';

const getExpireDate = (recordLifeTime: number): Date => {
  const date = new Date();

  date.setMilliseconds(date.getMilliseconds() + recordLifeTime);

  return date;
};

export default async (
  resourceManagementConfig: IResourceManagementWrapper,
  recordLifeTime: number,
  taskBody: TTaskBody
): Promise<void> => {
  const expiredAt = getExpireDate(recordLifeTime);
  const recordId = taskBody.id;
  const record = await db.getRecord(recordId);
  const participants = await getParticipantsByRecordId(resourceManagementConfig, record);
  const patch = {
    duration: taskBody.error ? '--:--:--' : taskBody.duration,
    status: taskBody.error ? RecordItemStatus.failed : RecordItemStatus.available,
    src: taskBody.src,
    size: `${taskBody.size}`,
    resolution: taskBody.resolution as string,
    expiredAt,
    type: taskBody.type as string,
  };

  await db.updateRecord(taskBody.id, patch);

  await getRecordingSubPub().publishToChannel({
    type: RecordingMessageType.RECORDING_UPDATED,
    record: { ...record, ...patch },
    participants,
  });
};
