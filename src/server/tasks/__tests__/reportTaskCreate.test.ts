import { TTaskBody } from '@s2m/cloud';
import reportTaskCreate from '../reportTaskCreate';
import { mock } from 'jest-mock-extended';
import { GrantType } from '@s2m/resource-management/lib';
import getParticipantsByRecordId from '../../utils/getParticipantsByRecord';
import { TRecordItem, IRecordingSubPub } from '@s2m/recording-db';
import db from '../../db';

/* eslint-disable max-statements, @typescript-eslint/unbound-method */

jest.mock('../../db');

const mockRecordingSubPub = mock<IRecordingSubPub>();

jest.mock('@s2m/recording-db/lib/getRecordingSubPub', () => ({
  default: () => mockRecordingSubPub,
}));

const getRealm = () => ({
  realmName: '',
  baseUrl: 'http://host/auth',
  grantType: GrantType.CLIENT_CREDENTIALS,
  clientId: 'clientId1',
  clientSecret: 'clientSecret',
});

jest.mock('@s2m/resource-management');
jest.mock('@s2m/cloud');
jest.mock('../../utils/getParticipantsByRecord');

const ONE_MONTH = 2629746;

describe('reportTaskCreate', () => {
  const taskBody = mock<TTaskBody>();
  const config = { getRealm, realms: {} };

  beforeEach(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();

    (db.getRecord as jest.Mock).mockResolvedValue({
      id: 'recordId1',
      src: 'recordSrc',
    } as TRecordItem);
    (getParticipantsByRecordId as jest.Mock).mockResolvedValue(['user1']);
  });

  test('test1', async () => {
    taskBody.id = 'recordId1';
    taskBody.error = null;

    await reportTaskCreate(config, ONE_MONTH, taskBody);

    expect(db.getRecord as jest.Mock).toBeCalledWith('recordId1');
    expect(getParticipantsByRecordId).toBeCalledWith(config, {
      id: 'recordId1',
      src: 'recordSrc',
    });
    expect(db.updateRecord).toBeCalled();
    expect(mockRecordingSubPub.publishToChannel).toBeCalled();
  });

  test('test2 with error', async () => {
    taskBody.id = 'recordId1';
    taskBody.error = 'error';

    await reportTaskCreate(config, ONE_MONTH, taskBody);

    expect(db.getRecord).toBeCalledWith('recordId1');
    expect(getParticipantsByRecordId).toBeCalledWith(config, {
      id: 'recordId1',
      src: 'recordSrc',
    });
    expect(db.updateRecord).toBeCalled();
    expect(mockRecordingSubPub.publishToChannel).toBeCalled();
  });
});
