import { TTaskBody, getStorage } from '@s2m/cloud';
import reportTaskRemove from '../reportTaskRemove';
import { mock } from 'jest-mock-extended';
import { GrantType, IResourceApi } from '@s2m/resource-management/lib';
import * as api from '@s2m/resource-management';
import getParticipantsByRecordId from '../../utils/getParticipantsByRecord';
import { TRecordItem, IRecordingSubPub } from '@s2m/recording-db';
import db from '../../db';

/* eslint-disable max-statements, @typescript-eslint/unbound-method */

const getRealm = () => ({
  realmName: '',
  baseUrl: 'http://host/auth',
  grantType: GrantType.CLIENT_CREDENTIALS,
  clientId: 'clientId1',
  clientSecret: 'clientSecret',
});

const mockRecordingSubPub = mock<IRecordingSubPub>();

jest.mock('@s2m/recording-db/lib/getRecordingSubPub', () => ({
  default: () => mockRecordingSubPub,
}));

jest.mock('@s2m/resource-management');
jest.mock('../../db');
jest.mock('@s2m/cloud');
jest.mock('../../utils/getParticipantsByRecord');

describe('reportTaskRemove', () => {
  test('test1', async () => {
    const taskBody = mock<TTaskBody>();
    const resourceApi = mock<IResourceApi>();
    const removeFromS3File = jest.fn();

    jest.spyOn(api, 'default').mockReturnValue(resourceApi);

    taskBody.id = 'recordId1';

    (db.getRecord as jest.Mock).mockResolvedValue({
      id: 'recordId1',
      src: 'recordSrc',
    } as TRecordItem);

    (getParticipantsByRecordId as jest.Mock).mockResolvedValue(['user1', 'user2']);
    (getStorage as jest.Mock).mockImplementation(() => ({
      removeFromS3File,
    }));
    const config = { getRealm, realms: {} };

    await reportTaskRemove({ getRealm, realms: {} }, taskBody);

    expect(db.getRecord).toBeCalledWith('recordId1');
    expect(getParticipantsByRecordId).toBeCalledWith(config, {
      id: 'recordId1',
      src: 'recordSrc',
    });
    expect(removeFromS3File).toBeCalledWith('recordSrc');
    expect(resourceApi.dissocResourceWithAllUsers).toBeCalled();
    expect(db.deleteRecord).toBeCalledWith('recordId1');
    expect(mockRecordingSubPub.publishToChannel).toBeCalled();
  });
});
