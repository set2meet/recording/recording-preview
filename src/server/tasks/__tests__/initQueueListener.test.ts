import reportTaskCreate from '../reportTaskCreate';
import reportTaskRemove from '../reportTaskRemove';
import { getQueue, TQueueTask, TaskActionType } from '@s2m/cloud';
import initQueueListener from '../initQueueListener';
import { mock } from 'jest-mock-extended';
import db from '../../db';
import { IDistributedLock } from '@s2m/recording-db';
import { GrantType } from '@s2m/resource-management';

/* eslint-disable max-statements,
@typescript-eslint/no-unsafe-assignment,
@typescript-eslint/no-unsafe-call,
@typescript-eslint/no-unsafe-member-access,
@typescript-eslint/unbound-method
*/

const getRealm = () => ({
  realmName: '',
  baseUrl: 'http://host/auth',
  grantType: GrantType.CLIENT_CREDENTIALS,
  clientId: 'clientId1',
  clientSecret: 'clientSecret',
});

const config = {
  recordingPreview: {
    nextTaskTimeout: 1000,
    searchTaskTimeout: 10000,
    waitTimeout: 10,
    visibilityTimeout: 600,
  },
  resourceManagement: { getRealm, realms: {} },
};

const mockDistributedLock = mock<IDistributedLock>();

jest.mock('@s2m/recording-db', () => ({
  getDistributedLock: () => mockDistributedLock,
}));

jest.mock('../reportTaskCreate');
jest.mock('../reportTaskRemove');
jest.mock('../../db');
jest.mock('@s2m/cloud');

describe('initQueueListener', () => {
  const consumerMock = {
    onTask: jest.fn(),
  };

  beforeEach(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();

    (getQueue as jest.Mock).mockImplementation(() => ({
      getConsumerForReport: () => consumerMock,
    }));
  });

  test('test create task', async () => {
    (db.isRecordExists as jest.Mock).mockResolvedValue(true);
    mockDistributedLock.acquireLock.mockResolvedValue(true);

    initQueueListener(config);

    const queueTask = mock<TQueueTask>();

    queueTask.desc.id = 'id1';
    queueTask.desc.action = TaskActionType.Create;

    const taskHandler = consumerMock.onTask.mock.calls[0][0];

    await taskHandler(queueTask);

    expect(db.isRecordExists).toBeCalledWith('id1');
    expect(reportTaskCreate).toBeCalled();
    expect(queueTask.done).toBeCalled();
  });

  test('test create task, key already locked', async () => {
    mockDistributedLock.acquireLock.mockResolvedValue(false);

    initQueueListener(config);

    const queueTask = mock<TQueueTask>();
    const taskHandler = consumerMock.onTask.mock.calls[0][0];

    await taskHandler(queueTask);

    expect(db.isRecordExists).not.toBeCalled();
    expect(reportTaskCreate).not.toBeCalled();
    expect(queueTask.done).not.toBeCalled();
    expect(queueTask.break).toBeCalled();
  });

  test('test create task, no recordings', async () => {
    (db.isRecordExists as jest.Mock).mockResolvedValue(false);
    mockDistributedLock.acquireLock.mockResolvedValue(true);

    initQueueListener(config);

    const queueTask = mock<TQueueTask>();

    queueTask.desc.id = 'id1';
    queueTask.desc.action = TaskActionType.Create;

    const taskHandler = consumerMock.onTask.mock.calls[0][0];

    await taskHandler(queueTask);

    expect(db.isRecordExists as jest.Mock).toBeCalledWith('id1');
    expect(reportTaskCreate).not.toBeCalled();
    expect(queueTask.done).toBeCalled();
  });

  test('test create task, with error', async () => {
    (db.isRecordExists as jest.Mock).mockResolvedValue(true);
    mockDistributedLock.acquireLock.mockResolvedValue(true);
    (reportTaskRemove as jest.Mock).mockImplementation(() => {
      throw new Error('error');
    });

    initQueueListener(config);

    const queueTask = mock<TQueueTask>();

    queueTask.desc.id = 'id1';
    queueTask.desc.action = TaskActionType.Remove;

    const taskHandler = consumerMock.onTask.mock.calls[0][0];

    await taskHandler(queueTask);

    expect(db.isRecordExists as jest.Mock).toBeCalledWith('id1');
    expect(reportTaskRemove).toBeCalled();
    expect(queueTask.done).not.toBeCalled();
    expect(queueTask.break).toBeCalled();
  });
});
