import { getQueue, TQueueTask, TaskActionType, TTaskBody } from '@s2m/cloud';
import db from '../db';
import { getDistributedLock } from '@s2m/recording-db';
import logger from '../../logger/logger';
import { LogErrorCode } from '../../logger/LogErrorCode';
import reportTaskCreate from './reportTaskCreate';
import reportTaskRemove from './reportTaskRemove';
import { TAppConfig } from '../types';
import { IRecordingPreviewConfig, IResourceManagementWrapper } from 'env-config';

const initLogger = logger.init;

const defaultConsumerConfig: TAppConfig = {
  nextTaskTimeout: 1000,
  searchTaskTimeout: 10000,
  waitTimeout: 10,
  visibilityTimeout: 600,
};

export type TQueueTaskHandler = (taskBody: TTaskBody) => Promise<void>;
const ONE_MONTH = 2629746;

//eslint-disable-next-line max-statements
const processTask = async (
  task: TQueueTask,
  resourceManagementConfig: IResourceManagementWrapper,
  recordLifetime: number
): Promise<void> => {
  const tasks: Record<string, TQueueTaskHandler> = {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    [TaskActionType.Create]: reportTaskCreate.bind(this, resourceManagementConfig, recordLifetime),
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    [TaskActionType.Remove]: reportTaskRemove.bind(this, resourceManagementConfig),
  };
  const desc = task.desc;
  const { id, action } = desc;

  initLogger.info(`start task ${action}`);

  try {
    const recordExists = await db.isRecordExists(id);

    if (recordExists) {
      await tasks[action](desc);
    } else {
      initLogger.info(`Cannot find record with id = ${id}`);
    }

    await task.done();

    initLogger.info(`finish task ${action}`);
  } catch (e) {
    initLogger.error(LogErrorCode.E_INIT_QUEUE, 'error');

    await task.break();
  }
};

const initQueueListener = (config: IRecordingPreviewConfig, recordLifetime: number = ONE_MONTH): void => {
  const consumerConfig: TAppConfig = Object.assign({}, defaultConsumerConfig, config.recordingPreview);
  const consumer = getQueue().getConsumerForReport({
    nextTaskTimeout: consumerConfig.nextTaskTimeout,
    searchTaskTimeout: consumerConfig.searchTaskTimeout,
    waitTimeout: consumerConfig.waitTimeout,
    visibilityTimeout: consumerConfig.visibilityTimeout,
  });

  initLogger.info('Add Listener to ConsumerForReport queue.');

  // eslint-disable-next-line @typescript-eslint/no-misused-promises
  consumer.onTask(async (task: TQueueTask) => {
    const id = task.desc.id;
    const isLockAdded = await getDistributedLock().acquireLock(id, consumerConfig.waitTimeout);

    if (isLockAdded) {
      await processTask(task, config.resourceManagement, recordLifetime);
    } else {
      await task.break();
    }
  });
};

export default initQueueListener;
