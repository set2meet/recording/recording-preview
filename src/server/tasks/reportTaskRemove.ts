import { TTaskBody, getStorage } from '@s2m/cloud';
import api from '@s2m/resource-management';
import { RecordingMessageType, getRecordingSubPub } from '@s2m/recording-db';
import db from '../db';
import logger from '../../logger/logger';
import getParticipantsByRecordId from '../utils/getParticipantsByRecord';
import { IResourceManagementWrapper } from 'env-config';

const reportLogger = logger.report;

export default async (resourceManagementConfig: IResourceManagementWrapper, taskBody: TTaskBody): Promise<void> => {
  const recordId = taskBody.id;
  const record = await db.getRecord(recordId);
  const realmName = record.realmName;
  const realm = resourceManagementConfig.getRealm(realmName);
  const participants = await getParticipantsByRecordId(resourceManagementConfig, record);

  reportLogger.debug(`remove record with id=${recordId} uri=${record.src}`);

  await getStorage().removeFromS3File(record.src);
  await api(realm).dissocResourceWithAllUsers(realm, recordId);
  await db.deleteRecord(recordId);

  await getRecordingSubPub().publishToChannel({
    type: RecordingMessageType.RECORDING_REMOVED,
    record,
    participants,
  });
};
