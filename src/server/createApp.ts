import Router from 'koa-router';
import { TAppContext } from './types';
import Koa from 'koa';
import errorHandler from './middleware/errorHandler';
import bodyParser from 'koa-bodyparser';
import cors from '@koa/cors';
import flow from 'lodash/flow';
import { IRecordingPreviewConfig } from 'env-config';

type TRoutes = ((router: Router) => Router)[];

const createApp = (registerRoutes: (config: IRecordingPreviewConfig) => TRoutes) => (
  config: IRecordingPreviewConfig
): Koa<TAppContext> => {
  const routes = registerRoutes(config);
  const app = new Koa<TAppContext>();
  const router = new Router();

  app.use(errorHandler);
  app.use(bodyParser());

  flow(routes)(router);

  app
    .use(cors())
    .use(router.allowedMethods())
    .use(router.routes());

  return app;
};

export default createApp;
