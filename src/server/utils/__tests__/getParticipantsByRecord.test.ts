import { TRecordItem } from '@s2m/recording-db';
import * as api from '@s2m/resource-management';
import { IResourceApi } from '@s2m/resource-management';
import { mock } from 'jest-mock-extended';
import getParticipantsByRecordId from '../getParticipantsByRecord';
import { TCred } from '@s2m/resource-management';
import { GrantType } from '@s2m/resource-management/lib';

jest.mock('@s2m/resource-management');

describe('getParticipantsByRecordId', () => {
  test('test1', async () => {
    const resourceApi = mock<IResourceApi>();
    const record = mock<TRecordItem>();
    const cred = {
      realmName: '',
      baseUrl: '',
      grantType: GrantType.CLIENT_CREDENTIALS,
      clientId: 'clientId',
      clientSecret: 'clientSecret',
    };
    const getRealm = () => cred;
    const realmConfig = { getRealm, realms: {} };

    record.realmName = 'realmName';
    record.id = 'id';

    resourceApi.fetchUserIdsByResourceId.mockResolvedValue(null);
    jest.spyOn(api, 'default').mockReturnValue(resourceApi);

    await getParticipantsByRecordId(realmConfig, record);

    //eslint-disable-next-line @typescript-eslint/unbound-method
    expect(resourceApi.fetchUserIdsByResourceId).toBeCalledWith(cred as TCred, 'id');
  });
});
