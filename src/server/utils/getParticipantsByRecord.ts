import { TRecordItem } from '@s2m/recording-db';
import api from '@s2m/resource-management';
import { IResourceManagementWrapper, TRealm } from 'env-config';

const getParticipantsByRecordId = async (
  resourceManagementConfig: IResourceManagementWrapper,
  record: TRecordItem
): Promise<string[]> => {
  const realmName = record.realmName;
  const recordId = record.id;
  const realm: TRealm = resourceManagementConfig.getRealm(realmName);

  return await api(realm).fetchUserIdsByResourceId(realm, recordId);
};

export default getParticipantsByRecordId;
