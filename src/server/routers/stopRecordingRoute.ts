import isAuthorized from '../middleware/isAuthorized';
import appendRealm from '../middleware/appendRealm';
import stopRecording from '../middleware/stopRecording';
import Router from 'koa-router';
import { IResourceManagementWrapper } from 'env-config';

const createRecordingRoute = (resourceManagementConfig: IResourceManagementWrapper) => (router: Router): Router =>
  router.put(
    '/recording-preview/recordings/:recordId/stop',
    appendRealm(resourceManagementConfig),
    isAuthorized,
    stopRecording(resourceManagementConfig)
  );

export default createRecordingRoute;
