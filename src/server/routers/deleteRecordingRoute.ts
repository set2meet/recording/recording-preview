import isAuthorized from '../middleware/isAuthorized';
import appendRealm from '../middleware/appendRealm';
import deleteRecording from '../middleware/deleteRecording';
import hasDeletePermissions from '../middleware/hasDeletePermissions';
import Router from 'koa-router';
import { IResourceManagementWrapper } from 'env-config';

const deleteRecordingRouter = (resourceManagementConfig: IResourceManagementWrapper) => (router: Router): Router =>
  router.delete(
    '/recording-preview/recordings/:recordId',
    appendRealm(resourceManagementConfig),
    isAuthorized,
    hasDeletePermissions,
    deleteRecording(resourceManagementConfig)
  );

export default deleteRecordingRouter;
