import Router from 'koa-router';
import checkHealth from '../middleware/checkHealth';

const healthRoute = (router: Router): Router => router.get('/health', checkHealth);

export default healthRoute;
