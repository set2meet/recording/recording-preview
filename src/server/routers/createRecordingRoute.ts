import isAuthorized from '../middleware/isAuthorized';
import appendRealm from '../middleware/appendRealm';
import createRecording from '../middleware/createRecording';
import Router from 'koa-router';
import { IResourceManagementWrapper } from 'env-config';

const createRecordingRoute = (resourceManagementConfig: IResourceManagementWrapper) => (router: Router): Router =>
  router.put(
    '/recording-preview/recordings/:recordId/create',
    appendRealm(resourceManagementConfig),
    isAuthorized,
    createRecording
  );

export default createRecordingRoute;
