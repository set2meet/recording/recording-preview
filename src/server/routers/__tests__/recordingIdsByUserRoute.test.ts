import request from 'supertest';
import recordingIdsByUserRoute from '../recordingIdsByUserRoute';
import jwt from 'jsonwebtoken';
import { GrantType, IResourceApi, GroupName } from '@s2m/resource-management';
import * as api from '@s2m/resource-management';
import { mock } from 'jest-mock-extended';
import { constants } from 'http2';
import Koa, { Context } from 'koa';
import createApp from '../../createApp';
import { TRecordItem } from '@s2m/recording-db';
import db from '../../db';

/* eslint-disable @typescript-eslint/unbound-method */

const getRealm = () => {
  return {
    realmName: '',
    baseUrl: 'http://host/auth',
    grantType: GrantType.CLIENT_CREDENTIALS,
    clientId: 'clientId1',
    clientSecret: 'clientSecret',
  };
};

jest.mock('../../db');

describe('recordingIdsByUserRoute', () => {
  let app: Koa<Context> = null;
  const realmConfig = { resourceManagement: { getRealm, realms: {} } };

  beforeEach(() => {
    app = createApp((config) => [recordingIdsByUserRoute(config.resourceManagement)])(realmConfig);
  });

  const token = jwt.sign(
    {
      iss: 'http://host/auth/realms/Realm1',
      azp: 'clientId1',
    },
    'secret'
  );
  const resourceApi = mock<IResourceApi>();

  beforeEach(() => {
    jest.clearAllMocks();
    jest.restoreAllMocks();
  });

  test('get recordings', async () => {
    resourceApi.validateToken.mockResolvedValue(true);
    resourceApi.fetchResourceNamesByUserId.mockResolvedValue([
      {
        name: 'resourceName1',
        groups: [GroupName.owners, GroupName.viewers],
      },
      {
        name: 'resourceName2',
        groups: [GroupName.viewers],
      },
    ]);

    (db.getRecord as jest.Mock)
      .mockResolvedValueOnce({ id: 'record1', createdBy: '1' } as TRecordItem)
      .mockResolvedValueOnce({ id: 'record2', createdBy: '2' } as TRecordItem);

    jest.spyOn(api, 'default').mockReturnValue(resourceApi);

    const response = await request(app.callback())
      .get('/recording-preview/users/userId1/recordings')
      .set('Authorization', `bearer ${token}`);

    expect(resourceApi.validateToken).toBeCalled();
    expect(db.getRecord).toBeCalled();
    expect(response.body).toEqual([
      {
        createdBy: '1',
        id: 'record1',
        meta: {
          isOwner: true,
          isViewer: true,
        },
      },
      {
        createdBy: '2',
        id: 'record2',
        meta: {
          isOwner: false,
          isViewer: true,
        },
      },
    ]);
    expect(response.status).toBe(constants.HTTP_STATUS_OK);
  });
});
