import request from 'supertest';
import { constants } from 'http2';
import healthRoute from '../healthRoute';
import createApp from '../../createApp';
import { IRecordingPreviewConfig } from 'env-config';

test('health check works', async () => {
  const app = createApp(() => [healthRoute])({} as IRecordingPreviewConfig);
  const response = await request(app.callback()).get('/health');

  expect(response.status).toBe(constants.HTTP_STATUS_OK);
  expect(response.text).toBe('ok');
});
