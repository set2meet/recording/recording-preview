import request from 'supertest';
import downloadRecordingRoute from '../downloadRecordingRoute';
import jwt from 'jsonwebtoken';
import { GrantType, IResourceApi } from '@s2m/resource-management';
import * as api from '@s2m/resource-management';
import { mock } from 'jest-mock-extended';
import getStreamReaderFromUrl from '../../middleware/utils/getStreamReaderFromUrl';
import mime from 'mime-types';
import { constants } from 'http2';
import Koa, { Context } from 'koa';
import createApp from '../../createApp';
import { getStorage } from '@s2m/cloud';

/* eslint-disable @typescript-eslint/ban-ts-comment,
@typescript-eslint/no-unsafe-call,
@typescript-eslint/unbound-method,
max-statements,
*/

const getRealm = () => {
  return {
    realmName: '',
    baseUrl: 'http://host/auth',
    grantType: GrantType.CLIENT_CREDENTIALS,
    clientId: 'clientId1',
    clientSecret: 'clientSecret',
  };
};

jest.mock('@s2m/cloud');
jest.mock('../../middleware/utils/getStreamReaderFromUrl');

describe('recording preview', () => {
  let app: Koa<Context> = null;
  const realmConfig = { resourceManagement: { getRealm, realms: {} } };

  beforeEach(() => {
    app = createApp((config) => [downloadRecordingRoute(config.resourceManagement)])(realmConfig);
  });

  const token = jwt.sign(
    {
      iss: 'http://host/auth/realms/Realm1',
      azp: 'clientId1',
    },
    'secret'
  );
  const resourceApi = mock<IResourceApi>();

  beforeEach(() => {
    jest.clearAllMocks();
    jest.restoreAllMocks();
  });

  test('token valid, access exits', async () => {
    resourceApi.validateToken.mockResolvedValue(true);
    resourceApi.hasReadPermissions.mockResolvedValue(true);

    (getStreamReaderFromUrl as jest.Mock).mockReturnValue('fileStream1');
    (getStorage as jest.Mock).mockImplementation(() => ({
      signedUrl: (uri: string): string => `async/${uri}`,
    }));

    jest.spyOn(api, 'default').mockReturnValue(resourceApi);

    const response = await request(app.callback())
      .get('/recording-preview?uri=root/path.mp4')
      .set('Authorization', `bearer ${token}`);

    expect(resourceApi.validateToken).toBeCalled();
    expect(resourceApi.hasReadPermissions).toBeCalled();
    expect(getStreamReaderFromUrl).toBeCalled();
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    expect(response.headers['content-type']).toBe(mime.types.mp4);
    expect((response.body as Buffer).toString()).toBe('fileStream1');
    expect(response.status).toBe(constants.HTTP_STATUS_OK);
  });

  test('token invalid', async () => {
    resourceApi.validateToken.mockResolvedValue(false);
    resourceApi.hasReadPermissions.mockResolvedValue(true);

    jest.spyOn(api, 'default').mockReturnValue(resourceApi);

    const response = await request(app.callback())
      .get('/recording-preview?uri=root/path.mp4')
      .set('Authorization', `bearer ${token}`);

    expect(resourceApi.validateToken).toBeCalled();
    expect(resourceApi.hasReadPermissions).not.toBeCalled();
    expect(getStreamReaderFromUrl).not.toBeCalled();
    expect(response.status).toBe(constants.HTTP_STATUS_UNAUTHORIZED);
  });

  test('token valid, no access', async () => {
    resourceApi.validateToken.mockResolvedValue(true);
    resourceApi.hasReadPermissions.mockResolvedValue(false);

    jest.spyOn(api, 'default').mockReturnValue(resourceApi);

    const response = await request(app.callback())
      .get('/recording-preview?uri=root/path.mp4')
      .set('Authorization', `bearer ${token}`);

    expect(resourceApi.validateToken).toBeCalled();
    expect(resourceApi.hasReadPermissions).toBeCalled();
    expect(getStreamReaderFromUrl).not.toBeCalled();
    expect(response.status).toBe(constants.HTTP_STATUS_FORBIDDEN);
  });
});
