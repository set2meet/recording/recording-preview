import request from 'supertest';
import createRecordingRoute from '../createRecordingRoute';
import jwt from 'jsonwebtoken';
import { GrantType, IResourceApi } from '@s2m/resource-management';
import * as api from '@s2m/resource-management';
import { mock } from 'jest-mock-extended';
import { constants } from 'http2';
import Koa, { Context } from 'koa';
import createApp from '../../createApp';
import db from '../../db';
import { RecordItemStatus } from '@s2m/recording-db';

/* eslint-disable
@typescript-eslint/no-unsafe-call,
@typescript-eslint/unbound-method,
max-statements,
*/

jest.mock('../../db');

const getRealm = () => {
  return {
    realmName: 'Realm1',
    baseUrl: 'http://host/auth',
    grantType: GrantType.CLIENT_CREDENTIALS,
    clientId: 'clientId1',
    clientSecret: 'clientSecret',
  };
};

describe('createRecordingRoute', () => {
  let app: Koa<Context> = null;
  const realmConfig = { resourceManagement: { getRealm, realms: {} } };

  beforeEach(() => {
    app = createApp((config) => [createRecordingRoute(config.resourceManagement)])(realmConfig);
  });

  const token = jwt.sign(
    {
      iss: 'http://host/auth/realms/Realm1',
      azp: 'clientId1',
      sub: 'userId',
      name: 'UserName',
    },
    'secret'
  );
  const resourceApi = mock<IResourceApi>();

  beforeEach(() => {
    jest.clearAllMocks();
    jest.restoreAllMocks();
  });

  test('should create resource and owners permissions', async () => {
    resourceApi.validateToken.mockResolvedValue(true);
    jest.spyOn(api, 'default').mockReturnValue(resourceApi);

    (db.updateRecord as jest.Mock).mockResolvedValue(null);

    const response = await request(app.callback())
      .put('/recording-preview/recordings/recordId1/create')
      .send({ roomId: 'roomId1' })
      .set('Authorization', `bearer ${token}`);

    expect(resourceApi.validateToken).toBeCalled();
    expect(db.updateRecord as jest.Mock).toBeCalledWith(
      'recordId1',
      expect.objectContaining({
        realmName: 'Realm1',
        status: RecordItemStatus.recording,
        roomId: 'roomId1',
        createdBy: 'userId',
      })
    );

    expect(resourceApi.assocResourceWithUsers).toBeCalledWith(
      expect.objectContaining({
        clientId: 'clientId1',
        clientSecret: 'clientSecret',
      }),
      'recordId1',
      expect.objectContaining({
        owners: ['userId'],
      })
    );
    expect(response.status).toBe(constants.HTTP_STATUS_OK);
  });
});
