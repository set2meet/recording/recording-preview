import request from 'supertest';
import stopRecordingRoute from '../stopRecordingRoute';
import jwt from 'jsonwebtoken';
import { GrantType, IResourceApi } from '@s2m/resource-management';
import * as api from '@s2m/resource-management';
import { mock } from 'jest-mock-extended';
import { constants } from 'http2';
import Koa, { Context } from 'koa';
import createApp from '../../createApp';
import { RecordItemStatus, RecordingMessageType, TRecordItem, IRecordingSubPub } from '@s2m/recording-db';
import getParticipantsByRecordId from '../../utils/getParticipantsByRecord';
import db from '../../db';

/* eslint-disable
@typescript-eslint/no-unsafe-call,
@typescript-eslint/unbound-method,
max-statements,
*/

const mockRecordingSubPub = mock<IRecordingSubPub>();

jest.mock('@s2m/recording-db/lib/getRecordingSubPub', () => ({
  default: () => mockRecordingSubPub,
}));

jest.mock('../../db');

const getRealm = () => {
  return {
    realmName: '',
    baseUrl: 'http://host/auth',
    grantType: GrantType.CLIENT_CREDENTIALS,
    clientId: 'clientId1',
    clientSecret: 'clientSecret',
  };
};

jest.mock('../../utils/getParticipantsByRecord');

describe('createRecordingRoute', () => {
  let app: Koa<Context> = null;
  const realmConfig = { resourceManagement: { getRealm, realms: {} } };

  beforeEach(() => {
    app = createApp((config) => [stopRecordingRoute(config.resourceManagement)])(realmConfig);
  });

  const token = jwt.sign(
    {
      iss: 'http://host/auth/realms/Realm1',
      azp: 'clientId1',
      sub: 'userId',
      name: 'UserName',
    },
    'secret'
  );
  const resourceApi = mock<IResourceApi>();

  beforeEach(() => {
    jest.clearAllMocks();
    jest.restoreAllMocks();
  });

  test('should change status to processing and publish event', async () => {
    resourceApi.validateToken.mockResolvedValue(true);
    jest.spyOn(api, 'default').mockReturnValue(resourceApi);

    (db.updateRecord as jest.Mock).mockResolvedValue(null);
    (db.getRecord as jest.Mock).mockResolvedValue({
      id: 'recordId1',
      src: 'recordSrc',
    } as TRecordItem);

    (getParticipantsByRecordId as jest.Mock).mockResolvedValue(['user1', 'user2']);

    const response = await request(app.callback())
      .put('/recording-preview/recordings/recordId1/stop')
      .set('Authorization', `bearer ${token}`);

    expect(resourceApi.validateToken).toBeCalled();
    expect(db.updateRecord).toBeCalledWith('recordId1', {
      status: RecordItemStatus.processing,
    });
    expect(mockRecordingSubPub.publishToChannel).toBeCalledWith({
      type: RecordingMessageType.RECORDING_UPDATED,
      record: {
        id: 'recordId1',
        src: 'recordSrc',
      },
      participants: ['user1', 'user2'],
    });
    expect(response.status).toBe(constants.HTTP_STATUS_OK);
  });
});
