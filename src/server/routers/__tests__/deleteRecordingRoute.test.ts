import request from 'supertest';
import deleteRecordingRoute from '../deleteRecordingRoute';
import jwt from 'jsonwebtoken';
import { GrantType, IResourceApi } from '@s2m/resource-management';
import * as api from '@s2m/resource-management';
import { mock } from 'jest-mock-extended';
import { constants } from 'http2';
import Koa, { Context } from 'koa';
import createApp from '../../createApp';
import getParticipantsByRecordId from '../../utils/getParticipantsByRecord';
import { RecordItemStatus, TRecordItem, IRecordingSubPub } from '@s2m/recording-db';
import { getQueue } from '@s2m/cloud';
import db from '../../db';

/* eslint-disable @typescript-eslint/ban-ts-comment,
@typescript-eslint/no-unsafe-call,
@typescript-eslint/unbound-method,
max-statements,
*/

const mockRecordingSubPub = mock<IRecordingSubPub>();

jest.mock('@s2m/recording-db/lib/getRecordingSubPub', () => ({
  default: () => mockRecordingSubPub,
}));

jest.mock('../../db');

const getRealm = () => {
  return {
    realmName: '',
    baseUrl: 'http://host/auth',
    grantType: GrantType.CLIENT_CREDENTIALS,
    clientId: 'clientId1',
    clientSecret: 'clientSecret',
  };
};

jest.mock('@s2m/cloud');
jest.mock('../../utils/getParticipantsByRecord');

describe('deleteRecordingRoute', () => {
  let app: Koa<Context> = null;
  const realmConfig = { resourceManagement: { getRealm, realms: {} } };

  beforeEach(() => {
    app = createApp((config) => [deleteRecordingRoute(config.resourceManagement)])(realmConfig);
  });

  const token = jwt.sign(
    {
      iss: 'http://host/auth/realms/Realm1',
      azp: 'clientId1',
    },
    'secret'
  );
  const resourceApi = mock<IResourceApi>();

  beforeEach(() => {
    jest.clearAllMocks();
    jest.restoreAllMocks();
  });

  test('token valid, access exits', async () => {
    resourceApi.validateToken.mockResolvedValue(true);
    resourceApi.hasDeletePermissions.mockResolvedValue(true);

    jest.spyOn(api, 'default').mockReturnValue(resourceApi);

    (db.getRecord as jest.Mock).mockResolvedValue({
      id: 'recordId1',
      src: 'recordSrc',
    } as TRecordItem);
    (db.updateRecord as jest.Mock).mockResolvedValue(null);

    (getParticipantsByRecordId as jest.Mock).mockResolvedValue(['user1', 'user2']);

    const createRecordTask = jest.fn();

    (getQueue as jest.Mock).mockImplementation(() => ({ createRecordTask }));

    const response = await request(app.callback())
      .del('/recording-preview/recordings/recordId1')
      .set('Authorization', `bearer ${token}`);

    expect(resourceApi.validateToken).toBeCalled();
    expect(resourceApi.hasDeletePermissions).toBeCalled();
    expect(db.getRecord).toBeCalledWith('recordId1');
    expect(db.updateRecord).toBeCalledWith('recordId1', {
      status: RecordItemStatus.removing,
    });
    expect(createRecordTask).toBeCalled();
    expect(mockRecordingSubPub.publishToChannel).toBeCalled();
    expect(response.status).toBe(constants.HTTP_STATUS_OK);
  });

  test('token invalid', async () => {
    resourceApi.validateToken.mockResolvedValue(false);
    resourceApi.hasDeletePermissions.mockResolvedValue(true);

    jest.spyOn(api, 'default').mockReturnValue(resourceApi);

    const response = await request(app.callback())
      .del('/recording-preview/recordings/recordId1')
      .set('Authorization', `bearer ${token}`);

    expect(resourceApi.validateToken).toBeCalled();
    expect(resourceApi.hasDeletePermissions).not.toBeCalled();
    expect(response.status).toBe(constants.HTTP_STATUS_UNAUTHORIZED);
  });

  test('token valid, no access', async () => {
    resourceApi.validateToken.mockResolvedValue(true);
    resourceApi.hasDeletePermissions.mockResolvedValue(false);

    jest.spyOn(api, 'default').mockReturnValue(resourceApi);

    const response = await request(app.callback())
      .del('/recording-preview/recordings/recordId1')
      .set('Authorization', `bearer ${token}`);

    expect(resourceApi.validateToken).toBeCalled();
    expect(resourceApi.hasDeletePermissions).toBeCalled();
    expect(response.status).toBe(constants.HTTP_STATUS_FORBIDDEN);
  });
});
