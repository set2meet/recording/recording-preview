import request from 'supertest';
import addParticipantToRecordingRoute from '../addParticipantToRecordingRoute';
import jwt from 'jsonwebtoken';
import { GrantType, IResourceApi } from '@s2m/resource-management';
import * as api from '@s2m/resource-management';
import { mock } from 'jest-mock-extended';
import { constants } from 'http2';
import Koa, { Context } from 'koa';
import createApp from '../../createApp';
import { TRecordItem } from '@s2m/recording-db';
import db from '../../db';

/* eslint-disable
@typescript-eslint/no-unsafe-call,
@typescript-eslint/unbound-method,
max-statements,
*/

jest.mock('../../db');

const getRealm = () => {
  return {
    realmName: 'Realm1',
    baseUrl: 'http://host/auth',
    grantType: GrantType.CLIENT_CREDENTIALS,
    clientId: 'clientId1',
    clientSecret: 'clientSecret',
  };
};

describe('addParticipantToRecordingRoute', () => {
  let app: Koa<Context> = null;
  const realmConfig = { resourceManagement: { getRealm, realms: {} } };

  beforeEach(() => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    app = createApp((config) => [addParticipantToRecordingRoute(config.resourceManagement)])(realmConfig);
  });

  const token = jwt.sign(
    {
      iss: 'http://host/auth/realms/Realm1',
      azp: 'clientId1',
      sub: 'userId',
      name: 'UserName',
    },
    'secret'
  );
  const resourceApi = mock<IResourceApi>();

  beforeEach(() => {
    jest.clearAllMocks();
    jest.restoreAllMocks();
  });

  test('should add participant as viewer', async () => {
    resourceApi.validateToken.mockResolvedValue(true);
    jest.spyOn(api, 'default').mockReturnValue(resourceApi);

    (db.getRecord as jest.Mock).mockResolvedValue({
      id: 'recordId1',
      src: 'recordSrc',
    } as TRecordItem);

    const response = await request(app.callback())
      .put('/recording-preview/recordings/recordId1/participant/add/userId1')
      .set('Authorization', `Bearer ${token}`);

    expect(resourceApi.validateToken).toBeCalled();
    expect(resourceApi.assocResourceWithUsers).toBeCalledWith(
      expect.objectContaining({
        clientId: 'clientId1',
        clientSecret: 'clientSecret',
      }),
      'recordId1',
      {
        viewers: ['userId1'],
      }
    );

    expect(response.status).toBe(constants.HTTP_STATUS_OK);
  });
});
