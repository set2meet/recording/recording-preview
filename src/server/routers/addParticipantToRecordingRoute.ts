import isAuthorized from '../middleware/isAuthorized';
import appendRealm from '../middleware/appendRealm';
import addParticipantToRecording from '../middleware/addParticipantToRecording';
import Router from 'koa-router';
import { IResourceManagementWrapper } from 'env-config';

const addParticipantToRecordingRoute = (resourceManagementConfig: IResourceManagementWrapper) => (
  router: Router
): Router =>
  router.put(
    '/recording-preview/recordings/:recordId/participant/add/:userId',
    appendRealm(resourceManagementConfig),
    isAuthorized,
    addParticipantToRecording
  );

export default addParticipantToRecordingRoute;
