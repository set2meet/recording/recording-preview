import isAuthorized from '../middleware/isAuthorized';
import appendRealm from '../middleware/appendRealm';
import Router from 'koa-router';
import getRecordingsForUser from '../middleware/getRecordingsForUser';
import { IResourceManagementWrapper } from 'env-config';

const recordingIdsByUserRouter = (resourceManagementConfig: IResourceManagementWrapper) => (router: Router): Router =>
  router.get(
    '/recording-preview/users/:userId/recordings',
    appendRealm(resourceManagementConfig),
    isAuthorized,
    getRecordingsForUser
  );

export default recordingIdsByUserRouter;
