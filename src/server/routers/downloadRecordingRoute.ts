import hasReadPermissions from '../middleware/hasReadPermissions';
import downloadRecording from '../middleware/downloadRecording';
import isAuthorized from '../middleware/isAuthorized';
import appendRealm from '../middleware/appendRealm';
import Router from 'koa-router';
import { IResourceManagementWrapper } from 'env-config';

const downloadRecordingRouter = (resourceManagementConfig: IResourceManagementWrapper) => (router: Router): Router =>
  router.get(
    '/recording-preview',
    appendRealm(resourceManagementConfig),
    isAuthorized,
    hasReadPermissions,
    downloadRecording
  );

export default downloadRecordingRouter;
