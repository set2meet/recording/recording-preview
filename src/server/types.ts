import { Context, DefaultState, ParameterizedContext } from 'koa';
import { TRealm } from 'env-config';
import { TParsedToken } from '@s2m/resource-management/lib/utils/parseToken';

export interface IQuery {
  uri: string;
}

export type TAppContext = ParameterizedContext<
  DefaultState & {
    realm?: TRealm;
    parsedToken?: TParsedToken;
    token?: string;
  },
  Context & {
    query: IQuery;
    params: {
      userId?: string;
      recordId?: string;
    };
  }
>;

export type TAppConfig = {
  nextTaskTimeout: number;
  searchTaskTimeout: number;
  waitTimeout: number;
  visibilityTimeout: number;
};
