const need_version = 12;
const cur_version = parseFloat(process.versions.node);

if (cur_version < need_version) {
  throw new Error(`Need node with version >= ${need_version} (current: ${cur_version})\n`);
}
