## Recording preview

#### Setup

1. see virtual-room readme   
2. run recording-preview locally
   1. npm i
   2. npm run server:dev

### Docker build params

For running this service through Docker you should provide options for env-config:
`-module {module-name} -stand {stand-name} -path {output-config-path} -url {hashicorp-url} -namespace {hashicorp-namespace} -role_id {CI-role-id} -secret_id {CI-secret-id} -npmScripts {script1, script2,...}`
where:
- `{module-name}`: name of the service (module) for which we want to build the config, without brackets
- `{stand-name}`: name of the stand for which we want config will be use, without brackets, or use `local` for local development
- `{output-config-path}`: path for your config
- `{CI-role-id}`: This attribute is for production/dev stands. This has been using inside CI/CD pipeline.
- `{CI-secret-id}`: This attribute is for production/dev stands. This has been using inside CI/CD pipeline.
- `{hashicorp-url}`: For custom hashicorp url.
- `{hashicorp-namespace}`: For custom hashicorp namespace.
- `{script1, script2,...}`: Array with _NPM_ scripts that will be executed after merging static and secret parts in memory. Inside code, it transforms into the: `concurrently "npm run script1" "npm run script2"... `
